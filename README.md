
### Notes
---
- Incoming damage is first reduced by the armor's damage resistance (`DR`), then whatever amount remains is further reduced by the damage threshold (`DT`).
    - Example: You are hit by a projectile dealing 60 damage while wearing `Advanced Powered Combat Armor`. Your `Advanced Powered Combat Armor` has a `DR` of 75%, so that damage is reduced by 75% to 15 (60 * (1.0 - 0.75) = 15). The armor has a `DT` of 11, so the remaining damage is reduced by 11 to a final value of 4 (15 - 11 = 4). Of the original 60 damage, you take 4 damage in all.
- The armor stats can be improved by four upgrades, shown below in the table.

### Stats

| Armor                         | DT (N) | DR (N) | DT (E)            | DR (E)                | Strength | Repair       |
|-------------------------------|--------|--------|-------------------|-----------------------|----------|--------------|
| Advanced Powered Combat Armor | 11     | 75%    | 11 / 11 / 11 / 11 | 95% / 60% / 90% / 95% | x2.25    | -            |
| Ceramic Plates (Upgrade)      | 13     | -      | 12 / 13 / 12 / 12 | -                     | -        | -            |
| Imp. Inner Lining (Upgrade)   | -      | 80%    | -                 | 95% / 60% / 95% / 95% | -        | -            |
| Imp. Servo-motors (Upgrade)   | -      | -      | -                 | -                     | x2.5     | -            |
| Nano Repair Module (Upgrade)  | -      | -      | -                 | -                     | -        | +2% / 5s (*) |

- (N) denotes normal damage
- (E) denotes elemental damage (fire, plasma, electric, and ice)
- (*) repair amount degrades with battery charge
